# Cron Parser & Renderer CLI

## Short Description
The Cron Parser & Renderer CLI is a command-line tool designed to parse and render Cron expressions. Crom expressions are a way to represent schedules in a readable and precise syntax. This tool allows users to input Cron expressions, process them, and display the resulting time interval.

## Installation

The tool is under [crates.io](https://crates.io/crates/).

## Usage
The Cron Parser & Renderer CLI is invoked via the command line. Here are some examples of usage:

- Parse a Cron expression and display the resulting time interval:

`cronista --crontab "* */10 5 4,5 1-3 /bash/script/`

```
+-------------+--------------+
| minute      | all times(*) |
+-------------+--------------+
| hour        | 0, 10, 20    |
+-------------+--------------+
| day         | 5            |
+-------------+--------------+
| Month       | 4,5          |
+-------------+--------------+
| day of week | 1, 2, 3      |
+-------------+--------------+
| Script      | /bash/script |
+-------------+--------------+
```

## Contribution
Contributions are welcome! If you'd like to suggest improvements or report bugs, simply create an issue or submit a pull request.

## License
This project is licensed under the MIT License. For more information, see the LICENSE file.
